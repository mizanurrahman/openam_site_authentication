package com.scholastic.idam.openam.site.main.pages;

import java.util.Iterator;
import java.util.Set;
import org.junit.Assert;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LogoutPage 
{
	private WebDriver driver=null;
	private Set<Cookie> cookies=null;
		
	 @FindBy(xpath="html/body/div[1]/div[2]/div[2]/div/div/h3")
	 private WebElement logoutText;


	 public LogoutPage(WebDriver driver)
	 {
	 	this.driver=driver;
	 }
	 
	 public void validateLogoutText()
	 {
		 try
		 {
			 logoutText.getText().equalsIgnoreCase("You are logged out.");
			 
		 }catch(NoSuchElementException e)
		 {
			 System.out.println(e.getMessage());
		 }
	 }
	 
	 public void validateEmptyCookies()
	 {
		 cookies=driver.manage().getCookies();
	     Iterator<Cookie> iterator=cookies.iterator();
	     try
	     {
	    	 System.out.println(iterator.next());
	    	 Assert.assertTrue(iterator.hasNext()==false);
	    	 System.out.println("Assertion passed");
	     }catch(Exception e)
	     {
	    	 System.out.println(e.getMessage());
	     }
	 }
}
