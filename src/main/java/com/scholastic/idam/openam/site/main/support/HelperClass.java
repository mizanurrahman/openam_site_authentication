package com.scholastic.idam.openam.site.main.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.scholastic.idam.openam.site.main.BasePage;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

public class HelperClass 
{
	public List<String>authnStrategies=new ArrayList<String>();
	public List<String>allowedIpAddresses=new ArrayList<String>();
	public List<String>allowedReferrers=new ArrayList<String>();
	public List<String>allowedLibraryCards=new ArrayList<String>();
	public String authnStrategy;
	private String siteID;
	public static String cookie;
	
	public String createSiteAuthenticationOnlyWithIpAddressStrategyTest(String stratregy)
	{
		authnStrategy=stratregy;
				
		//Create Site Authentication
		ExtractableResponse<Response> createSiteAuthenticationResponse=
				
							given()
									.log().all()
									.contentType("application/json")
									.body(createSiteAuthenticationWithIpAddressStrategyPayload()).
							when()
									.post(BasePage.getTestURL("ENDPOINT_CREATE_SITE_AUTHENTICAION")).
							then()
									.statusCode(201)
									.extract();
		System.out.println("!!!!!!!!!!"+createSiteAuthenticationResponse.asString());	
		siteID=createSiteAuthenticationResponse.path("id");
		System.out.println("Site ID Is:"+siteID);
		return siteID;	
	}
	
	public String createSiteAuthenticationOnlyWithReferrerUrlAndLibraryCardStrategyTest(String stratregy)
	{
		authnStrategy=stratregy;
				
		//Create Site Authentication
		ExtractableResponse<Response> createSiteAuthenticationResponse=
				
							given()
									.log().all()
									.contentType("application/json")
									.body(createSiteAuthenticationWithOnlyReferrerUrlAndLibraryCardPayload()).
							when()
									.post(BasePage.getTestURL("ENDPOINT_CREATE_SITE_AUTHENTICAION")).
							then()
									.statusCode(201)
									.extract();
		System.out.println("!!!!!!!!!!"+createSiteAuthenticationResponse.asString());	
		siteID=createSiteAuthenticationResponse.path("id");
		System.out.println("###################### " +siteID);
		return siteID;
	}
	public String createSiteAuthenticationOnlyWithReferrerUrlAndLibraryCardStrategyProductPageTest(String stratregy)
	{
		authnStrategy=stratregy;
		//Create Site Authentication
		ExtractableResponse<Response> createSiteAuthenticationResponse=
				
							given()
									.log().all()
									.contentType("application/json")
									.body(createSiteAuthenticationWithOnlyReferrerUrlAndLibraryCardProductPagePayload()).
							when()
									.post(BasePage.getTestURL("ENDPOINT_CREATE_SITE_AUTHENTICAION")).
							then()
									.statusCode(201)
									.extract();
		System.out.println("!!!!!!!!!!"+createSiteAuthenticationResponse.asString());	
		siteID=createSiteAuthenticationResponse.path("id");
		System.out.println("###################### " +siteID);
		return siteID;
	}
	public String createSiteAuthenticationOnlyWithReferrerUrlStrategyTest(String stratregy)
	{
		authnStrategy=stratregy;
				
		//Create Site Authentication
		ExtractableResponse<Response> createSiteAuthenticationResponse=
				
							given()
									.log().all()
									.contentType("application/json")
									.body(createSiteAuthenticationWithOnlyReferrerUrlPayload()).
							when()
									.post(BasePage.getTestURL("ENDPOINT_CREATE_SITE_AUTHENTICAION")).
							then()
									.statusCode(201)
									.extract();
		System.out.println("!!!!!!!!!!"+createSiteAuthenticationResponse.asString());	
		siteID=createSiteAuthenticationResponse.path("id");
		System.out.println("###################### " +siteID);
		return siteID;
	}
	
	public String createSiteAuthenticationOnlyWithReferrerUrlStrategyProductPageTest(String stratregy)
	{
		authnStrategy=stratregy;
				
		//Create Site Authentication
		ExtractableResponse<Response> createSiteAuthenticationResponse=
				
							given()
									.log().all()
									.contentType("application/json")
									.body(createSiteAuthenticationWithOnlyReferrerUrlProductPagePayload()).
							when()
									.post(BasePage.getTestURL("ENDPOINT_CREATE_SITE_AUTHENTICAION")).
							then()
									.statusCode(201)
									.extract();
		System.out.println("!!!!!!!!!!"+createSiteAuthenticationResponse.asString());	
		siteID=createSiteAuthenticationResponse.path("id");
		System.out.println("###################### " +siteID);
		return siteID;
	}
	
	private Map<String, Object> createSiteAuthenticationWithIpAddressStrategyPayload()
	{
		Map<String, Object> siteAuthenticationInfo = new HashMap<String, Object>();
		siteAuthenticationInfo.put("siteName", BasePage.readProperty("siteName"));
		siteAuthenticationInfo.put("orgUcn", BasePage.readProperty("orgUcn"));
		siteAuthenticationInfo.put("status", BasePage.readProperty("status"));
		authnStrategies.add(authnStrategy);
		siteAuthenticationInfo.put("authnStrategies", authnStrategies);
		allowedIpAddresses.add(UtilsMethods.GetLocalMachineIPAddress());
		siteAuthenticationInfo.put("allowedIpAddresses", allowedIpAddresses);
		System.out.println(siteAuthenticationInfo);
		return siteAuthenticationInfo;
	}
	
	@SuppressWarnings("rawtypes")
	private Map<String, Object> createSiteAuthenticationWithOnlyReferrerUrlAndLibraryCardPayload()
	{
		Map<String, Object> siteAuthenticationInfo = new HashMap<String, Object>();
		siteAuthenticationInfo.put("siteName", BasePage.readProperty("siteName"));
		siteAuthenticationInfo.put("orgUcn", BasePage.readProperty("orgUcn"));
		siteAuthenticationInfo.put("status", BasePage.readProperty("status"));
		authnStrategies.add(authnStrategy);
		siteAuthenticationInfo.put("authnStrategies", authnStrategies);
		Map<String, Object> allowedReferrers = new HashMap<String, Object>();
		allowedReferrers.put("domain",BasePage.readProperty("domain"));
		allowedReferrers.put("includeSubDomains",BasePage.readProperty("includeSubDomains"));
		List<Map> allowedReferrersList = new ArrayList<Map>();
		allowedReferrersList.add(allowedReferrers);
		siteAuthenticationInfo.put("allowedReferrers", allowedReferrersList);
		Map<String, Object> allowedLibraryCards = new HashMap<String, Object>();
		allowedLibraryCards.put("prefix",BasePage.readProperty("prefix"));
		allowedLibraryCards.put("length",BasePage.readProperty("length"));
		List<Map> allowedLibraryCardsList = new ArrayList<Map>();
		allowedLibraryCardsList.add(allowedLibraryCards);
		siteAuthenticationInfo.put("allowedLibraryCards", allowedLibraryCardsList);
		return siteAuthenticationInfo;
	}
	
	@SuppressWarnings("rawtypes")
	private Map<String, Object> createSiteAuthenticationWithOnlyReferrerUrlAndLibraryCardProductPagePayload()
	{
		Map<String, Object> siteAuthenticationInfo = new HashMap<String, Object>();
		siteAuthenticationInfo.put("siteName", BasePage.readProperty("siteName"));
		siteAuthenticationInfo.put("orgUcn", BasePage.readProperty("orgUcn"));
		siteAuthenticationInfo.put("status", BasePage.readProperty("status"));
		authnStrategies.add(authnStrategy);
		siteAuthenticationInfo.put("authnStrategies", authnStrategies);
		Map<String, Object> allowedReferrers = new HashMap<String, Object>();
		allowedReferrers.put("domain",BasePage.readProperty("domain_product"));
		allowedReferrers.put("includeSubDomains",BasePage.readProperty("includeSubDomains"));
		List<Map> allowedReferrersList = new ArrayList<Map>();
		allowedReferrersList.add(allowedReferrers);
		siteAuthenticationInfo.put("allowedReferrers", allowedReferrersList);
		Map<String, Object> allowedLibraryCards = new HashMap<String, Object>();
		allowedLibraryCards.put("prefix",BasePage.readProperty("prefix"));
		allowedLibraryCards.put("length",BasePage.readProperty("length"));
		List<Map> allowedLibraryCardsList = new ArrayList<Map>();
		allowedLibraryCardsList.add(allowedLibraryCards);
		siteAuthenticationInfo.put("allowedLibraryCards", allowedLibraryCardsList);
		return siteAuthenticationInfo;
	}
	@SuppressWarnings("rawtypes")
	private Map<String, Object> createSiteAuthenticationWithOnlyReferrerUrlPayload()
	{
		Map<String, Object> siteAuthenticationInfo = new HashMap<String, Object>();
		siteAuthenticationInfo.put("siteName", BasePage.readProperty("siteName"));
		siteAuthenticationInfo.put("orgUcn", BasePage.readProperty("orgUcn"));
		siteAuthenticationInfo.put("status", BasePage.readProperty("status"));
		authnStrategies.add(authnStrategy);
		siteAuthenticationInfo.put("authnStrategies", authnStrategies);
		Map<String, Object> allowedReferrers = new HashMap<String, Object>();
		allowedReferrers.put("domain",BasePage.readProperty("domain"));
		allowedReferrers.put("includeSubDomains",BasePage.readProperty("includeSubDomains"));
		List<Map> allowedReferrersList = new ArrayList<Map>();
		allowedReferrersList.add(allowedReferrers);
		siteAuthenticationInfo.put("allowedReferrers", allowedReferrersList);
		return siteAuthenticationInfo;
	}
	@SuppressWarnings("rawtypes")
	private Map<String, Object> createSiteAuthenticationWithOnlyReferrerUrlProductPagePayload()
	{
		Map<String, Object> siteAuthenticationInfo = new HashMap<String, Object>();
		siteAuthenticationInfo.put("siteName", BasePage.readProperty("siteName"));
		siteAuthenticationInfo.put("orgUcn", BasePage.readProperty("orgUcn"));
		siteAuthenticationInfo.put("status", BasePage.readProperty("status"));
		authnStrategies.add(authnStrategy);
		siteAuthenticationInfo.put("authnStrategies", authnStrategies);
		Map<String, Object> allowedReferrers = new HashMap<String, Object>();
		allowedReferrers.put("domain",BasePage.readProperty("domain_product"));
		allowedReferrers.put("includeSubDomains",BasePage.readProperty("includeSubDomains"));
		List<Map> allowedReferrersList = new ArrayList<Map>();
		allowedReferrersList.add(allowedReferrers);
		siteAuthenticationInfo.put("allowedReferrers", allowedReferrersList);
		return siteAuthenticationInfo;
	}
	public void setCookieValue(String cookie)
	{
		HelperClass.cookie=cookie;
	}
	
	public void validateCookiesAttributes(String siteId,String stratregy)
	{
		ExtractableResponse<Response> cookieResponse=
				given()
						.log().all()
						.param("subjectid", HelperClass.cookie)
						.param("attributenames", "authnStrategy")
						.param("attributenames", "sessionType")
						.param("attributenames", "siteId")
						.param("attributenames", "orgUCN")
						.param("attributenames", stratregy).
				when()
						.get(BasePage.getTestURL("ENDPOINT_COOKIES_CONTENT")).
				then()
						.statusCode(200)
						.body(containsString("userdetails.token.id="+HelperClass.cookie))
						.body(containsString("userdetails.attribute.name=authnStrategy"))
						.body(containsString("userdetails.attribute.value="+authnStrategy))
						.body(containsString("userdetails.attribute.name=sessionType"))
						.body(containsString("userdetails.attribute.value=site"))
						.body(containsString("userdetails.attribute.name=siteId"))
						.body(containsString("userdetails.attribute.value="+siteId))
						.body(containsString("userdetails.attribute.name=orgUCN"))
						.body(containsString("userdetails.attribute.value="+BasePage.readProperty("orgUcn")))
						.extract();
								
		System.out.println(cookieResponse.asString());

	}
}
