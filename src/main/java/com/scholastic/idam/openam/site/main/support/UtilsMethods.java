package com.scholastic.idam.openam.site.main.support;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.scholastic.idam.openam.site.main.BasePage;

public class UtilsMethods 
{
	public static WebDriver getDriverType(String Browser)
	{
		if(Browser.equalsIgnoreCase("FireFox"))
		{
			System.setProperty("webdriver.gecko.driver",BasePage.FirefoxDriver);
			System.out.println("Opening Firefox Browser");
			return new FirefoxDriver();
		}else if(Browser.equalsIgnoreCase("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver",BasePage.ChromeDriverPath);
			System.out.println("Opening Chrome Browser");
			return new ChromeDriver();
		}else if(Browser.equalsIgnoreCase("IE"))
		{
			System.setProperty("webdriver.ie.driver", BasePage.IEDriverDriverPath);
			System.out.println("Opening IE Browser");
			return new InternetExplorerDriver();
		}else
			return null;
	}
	
	
		 
    public static String GetLocalMachineIPAddress()
    {
     try 
        {
            InetAddress ipAddr = InetAddress.getLocalHost();
             return ipAddr.getHostAddress();
        } catch (UnknownHostException ex) 
        {
            ex.printStackTrace();
            return null;
        }
    }
	
}
