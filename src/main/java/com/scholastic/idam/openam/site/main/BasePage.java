package com.scholastic.idam.openam.site.main;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import com.scholastic.idam.openam.site.main.support.UtilsMethods;

public class BasePage 
{
	public static String configFilepath=System.getProperty("user.dir")+"/src/main/resources/PropertiesFile/dev.properties";
	public static String ChromeDriverPath=System.getProperty("user.dir")+"/src/test/resources/Drivers/chromedriver.exe";
	public static String IEDriverDriverPath=System.getProperty("user.dir")+"/src/test/resources/Drivers/IEDriverServer.exe";
	public static String FirefoxDriver=System.getProperty("user.dir")+"/src/test/resources/Drivers/geckodriver.exe";
	public static Properties CONFIG=null;
	public static String browserType="Firefox";
	
	private static FileInputStream fin=null;
	private static boolean initial=false;
	public static  WebDriver driver;
	
	public static String siteID;
			
	public static void init()
	{
		System.out.println("INIT !!!!!!!!!!!!!!!!!!!!!!");
		if(!initial)
		{	
			CONFIG=new Properties();
			
			try
			{			
				fin=new FileInputStream(configFilepath);
				CONFIG.load(fin);
			}catch(IOException e)
			{
				System.out.println("File Not Found");
			}
		}
		initial=true;
	}
	
	public static void openBrowser()
	{
		driver=UtilsMethods.getDriverType(browserType);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	public static void closeBrowser() throws InterruptedException
	{
		Thread.sleep(5000L);
		driver.quit();
	}
	
	public static String getTestURL(String key)
	{
		return CONFIG.getProperty(key);	
	}
	
	public static String readProperty(String key)
	{
		return CONFIG.getProperty(key);	
	}
}
