package com.scholastic.idam.openam.site.main.pages;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LibraryCardPage 
{
	private WebDriver driver;
   
	
	@FindBy(xpath="//input[@id='IDToken1']")
 	private WebElement TBX1;
	
	@FindBy(xpath="//input[@name='Login.Submit']")
	private WebElement BTN;
	
	@FindBy(xpath="html/body/div[1]/div[2]/div[2]/div[2]/h1")
	private WebElement Text;
	
	@FindBy(xpath="html/body/a")
	private WebElement LibraryPageLink;
	
	
	public void nevigateToURL(String application)
 	{
	    driver.navigate().to(application);
	    
 	}
	
	public void goToLibraryPage()
	{
		LibraryPageLink.click();
	}
	
	public LibraryCardPage (WebDriver driver)
 	{
 		this.driver=driver;
 	}
	
	public void enterLibraryCard(String cardNumber)
	{
		TBX1.sendKeys(cardNumber);
	}
	
	public void clickLogInButton()
	{
		BTN.click();
	}
	
	public void validateLibraryCardPage()
	{
		try{
			
			Assert.assertTrue(Text.getText().equalsIgnoreCase("My Trail Site"));
			
		}catch(Exception e)
		{
			System.out.println("Assertion fail");
		}
	}
}
