package com.scholastic.idam.openam.site.main.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage 
{
	private WebDriver driver;
	
	@FindBy(xpath="html/body/div[2]/div[1]/div[2]/div[1]/h1")
	private WebElement Text;
	
	@FindBy(xpath="html/body/a")
	private WebElement ProductPageLink;
	
	public ProductPage(WebDriver driver)
 	{
 		this.driver=driver;
 	}
	
	public void nevigateToURL(String application)
 	{
	    driver.navigate().to(application);
	    
 	}
	public void goToProductPage()
	{
		ProductPageLink.click();
	}
	public void validateProductPage()
	{
		try{
			
			Assert.assertTrue(Text.getText().equalsIgnoreCase("Scholastic Digital Manager"));
			
		}catch(Exception e)
		{
			System.out.println("Assertion fail");
		}
	}
}
