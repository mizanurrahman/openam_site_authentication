package com.scholastic.idam.openam.site.main.pages;
import java.awt.Robot;
import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class LoginPage 
{
	Robot robot=null;
    private WebDriver driver;
    private Set<Cookie> cookies=null;
    private Cookie cookie=null;
    private String OpenAMSession="iPlanetDirectoryPro";
	
        
    @FindBy(xpath="html/body/form/div[2]/table[1]/tbody/tr/td[2]/a")
 	private WebElement logoutButton;
 	
 	@FindBy(xpath="html/body/form/table[2]/tbody/tr/td[2]/div/input[1]")
 	private WebElement saveButton;
 	
 	@FindBy(xpath="html/body/form/table[2]/tbody/tr/td[2]/div/input[2]")
 	private WebElement resetButton;
 	
 	@FindBy(xpath="html/body/form/table[2]/tbody/tr/td[1]/div/h1")
 	private WebElement siteId;
 	
 	@FindBy(xpath="html/body/a")
	private WebElement LoginPageLink;

 	 	
 	public LoginPage(WebDriver driver)
 	{
 		this.driver=driver;
 	}
 	public void goToLoginPage()
	{
		LoginPageLink.click();
	}
 	 	
 	public void nevigateToURL(String application)
 	{
	    driver.navigate().to(application);
	    
 	}
 	
 	public void validateLogoutButton()
 	{
 		try
 		{
 			Assert.assertTrue(logoutButton.isDisplayed());
 		}catch(Exception e)
 		{
 			System.out.println("Log out button is not diplaying");
 		}
 	}
 	
 	public void validateSaveButton()
 	{
 		try
 		{
 			Assert.assertTrue(saveButton.isDisplayed());
 		}catch(Exception e)
 		{
 			System.out.println("Save button is not diplaying");
 		}
 	}
 	
 	public void validateResetButton()
 	{
 		try
 		{
 			Assert.assertTrue(resetButton.isDisplayed());
 		}catch(Exception e)
 		{
 			System.out.println("Reset button is not diplaying");
 		}
 	}
 	
 	public void validateSiteID(String siteID)
 	{
 		try
 		{
 			Assert.assertTrue(siteId.getText().trim().equals(siteID));
 		}catch(Exception e)
 		{
 			System.out.println("Wrong Site ID");
 		}
 	}
 	
 	public void doLogout() throws InterruptedException 
 	{
 		logoutButton.click();
 		Thread.sleep(2000L);
 		driver.switchTo().alert().accept();
 		System.out.println(driver.getCurrentUrl());
 	}
 	
 	public String getCookie()
 	{
	  
		cookies=driver.manage().getCookies();
        Iterator<Cookie> iterator=cookies.iterator();
        System.out.println("Looking Cookies");
        while(iterator.hasNext())
        {
        	cookie=iterator.next();
        		 	
        	if(cookie.getName().equalsIgnoreCase(OpenAMSession))
        	{
        		System.out.println("Open AM session has created");
        		Assert.assertTrue(cookie.getDomain().equals(".devtest.com"));
        		return cookie.getValue();
        	}
        }
        return null;
 	}
}
