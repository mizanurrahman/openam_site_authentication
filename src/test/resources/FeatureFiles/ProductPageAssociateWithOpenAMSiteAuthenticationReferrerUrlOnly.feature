Feature: As a QA I want to Automate Product Page associate with the Open_Am Site Authentication ReferrerUrl Strategy Only Test
   In Order to reduce manual testing effort.

  Scenario: Product Page Associate With The Open_Am Site Authentication ReferrerUrl Strategy Only
    Given I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API for the Product Page of ReferrerUrl Strategy Only
    When I Invoke Open-Am Authentication ReferrerUrl and Library Card Strategy Only URL With Site Authentication ID
    And I Click on Go To The Link Of Library Card
    Then I Should Get the Scholastic Digital Manager Page
