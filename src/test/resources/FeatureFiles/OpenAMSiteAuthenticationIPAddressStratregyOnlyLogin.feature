Feature: As a QA I want to Automate Open_Am Site Authentication IP Address Strategy Only Test
   In Order to reduce manual testing effort.

  Scenario: Open_Am Site Authentication IP Address Strategy Only
    Given I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API
    When I Invoke Open-Am Authentication IP Address Only URL with Site Authentication ID
    Then I Should See Log out and Save and Reset button and Site ID after Successfully Log in
    And I should Get Cookie From Browser
    When I Will Call Open AM Attribute Api with the Browser Cookies
    Then I should get Those ipAddress Only Strategy attributes name and value in response
