Feature: As a QA I want to Automate Open_Am Site Authentication ReferrerUrl and Library Card Strategy Only Test
   In Order to reduce manual testing effort.

  Scenario: Open_Am Site Authentication ReferrerUrl and Library Card Strategy Only
    Given I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API for referrer URL and Library Card
    When I Invoke Open-Am Authentication Referrer Url and Library Card Only Strategy URL With Site ID
    And I Click on Go To Library Card Link
    Then I Should See The Library Card Entry Form
    And I Enter Library Card Number In the Library card number Field
    And Click lOG in button
    Then I Should See Log out and Save and Reset button and Site ID after Successfully log in
    And I should Get Cookie From the Browser
    When I Will Call Open AM Attribute Api along with the Browser Cookies
    Then I should get Those referrer Url and Library Card strategy attributes name and value in response
