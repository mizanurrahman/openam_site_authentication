Feature: As a QA I want to Automate Product Page associate with the Open_Am Site Authentication IP Address Stratregy Only Test
   In Order to reduce manual testing effort.

  Scenario: Product Page Associate With The Open_Am Site Authentication IP Address Strategy Only
    Given I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API for the Product Page of IP Address Strategy Only
    When I Invoke Open-Am Authentication IP Address Only Product Page URL with Site Authentication ID
    Then I Should See the Scholastic Digital Manager Page
