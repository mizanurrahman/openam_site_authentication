Feature: As a QA I want to Automate Open_Am Site Authentication ReferrerUrl Strategy Only Test
   In Order to reduce manual testing effort.

  Scenario: Open_Am Site Authentication ReferrerUrl Strategy Only
    Given I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API for referrer URL
    When I Invoke Open-Am Authentication ReferrerUrl Strategy Only With Site Authentication ID
    And I Click on Go To Referrer URL Link
    Then I will See Log out and Save and Reset button and Site ID after Successfully log in
    And I should Have Cookies From the Browser
    When I Am Going to Call Open AM Attribute Api along with the Browser Cookies
    Then I should get Those referrer Url strategy attributes name and value in response
