Feature: As a QA I want to Automate Product Page associate with the Open_Am Site Authentication ReferrerUrlAndLibrayCard Stratregy Only Test
   In Order to reduce manual testing effort.

  Scenario: Product Page Associate With The Open_Am Site Authentication ReferrerUrlAndLibraryCard Stratregy Only
    Given I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API for the Product Page of ReferrerUrlAndLibraryCard Stratregy Only
    When I Invoke Open-Am Authentication Product Page ReferrerUrlAndLibraryCard Strategy Only API With Site Authentication ID
    And I Click on Go To Referrer URL And Library Card Only Link
    Then I Should Have The Library Card Entry Form
    And I Insert Library Card Name In the Library card name Field
    And I Click lOG in button
    Then I Should Have the Scholastic Digital Manager Page
