package com.rough;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Test;
import com.jayway.restassured.response.ExtractableResponse;
import static org.hamcrest.Matchers.containsString;
import com.jayway.restassured.response.Response;

public class Rough 
{
	private static final String ENDPOINT_COOKIES_CONTENT=" http://openam.devtest.com:8080/openam/identity/attributes";
	String cookie="AQIC5wM2LY4SfczGY4pvUvlszMXmNytpG2Qge1jDR-nB5EI.*AAJTSQACMDEAAlNLABM2NDQ5MDcyMjA2MzY2NjIxMjAw*";
	String xx="30177095700934686";
	
	@Test
	public void getCookiesAttributes()
	{
		ExtractableResponse<Response> cookieResponse=
				given()
						.log().all()
						.param("subjectid", cookie)
						.param("attributenames", "sessionType")
						.param("attributenames", "siteId")
						.param("attributenames", "orgUCN")
						.param("attributenames", "authnStrategy").
				when()
						.get(ENDPOINT_COOKIES_CONTENT).
				then()
						.statusCode(200)
						.body(containsString("userdetails.attribute.name=sessionType"))
						.body(containsString("userdetails.attribute.value="+xx))
						.extract();
						
								
		System.out.println(cookieResponse.asString());

	}
	
	

}
