package com.scholastic.idam.openam.site.authentication.test.definations;

import org.openqa.selenium.support.PageFactory;

import com.scholastic.idam.openam.site.main.BasePage;
import com.scholastic.idam.openam.site.main.pages.LogoutPage;

import cucumber.api.java.en.Then;


public class Step_Def_Site_Auth_IP_Adress_Stratregy_Only_LogOut_Test 
{

public LogoutPage logoutPage;
	
	@Then("^I click on log out button$")
	public void i_click_on_log_out_button() throws Throwable
	{
		Step_Def_Site_Auth_IP_Address_Strategy_Only_LogIn_Test.loginPage.doLogout();
		logoutPage=PageFactory.initElements(BasePage.driver,LogoutPage.class);
	}
	
	@Then("^I should log out successfully$")
	public void i_should_log_out_successfully() throws Throwable 
	{
		logoutPage.validateLogoutText();
	}


	@Then("^I should see empty Cookies$")
	public void i_should_see_empty_Cookies() throws Throwable 
	{
   
		logoutPage.validateEmptyCookies();
	}
}
