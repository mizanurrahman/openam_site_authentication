package com.scholastic.idam.openam.site.authentication.test.definations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.idam.openam.site.main.BasePage;
import com.scholastic.idam.openam.site.main.pages.LibraryCardPage;
import com.scholastic.idam.openam.site.main.pages.ProductPage;
import com.scholastic.idam.openam.site.main.support.HelperClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Step_Def_Product_Page_Associate_With_The_Site_Auth_ReferrerUrl_And_LibraryCard_Strategy_Only_Test 
{
	public WebDriver driver=null;
	public LibraryCardPage libraryPage=null;
	public HelperClass helperClass=new HelperClass();
	public static ProductPage productPage;
	public static final String STRATREGY="referrerUrlAndLibraryCardStrategy";
	
	@Given("^I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API for the Product Page of ReferrerUrlAndLibraryCard Stratregy Only$")
	public void i_Am_Creating_Open_Am_Site_Authentication_ID_using_IAM_Site_Authentication_API_for_the_Product_Page_of_ReferrerUrlAndLibraryCard_Stratregy_Only()
	{
		helperClass=new HelperClass();
		BasePage.siteID=helperClass.createSiteAuthenticationOnlyWithReferrerUrlAndLibraryCardStrategyProductPageTest(STRATREGY); 
	    
	}

	@When("^I Invoke Open-Am Authentication Product Page ReferrerUrlAndLibraryCard Strategy Only API With Site Authentication ID$")
	public void i_Invoke_Open_Am_Authentication_Product_Page_ReferrerUrlAndLibraryCard_Strategy_Only_API_With_Site_Authentication_ID_Using_Browser()
	{
		System.out.println("Product Page - ReferrerUrlOnlyAndLibraryCard Strategy");
		BasePage.openBrowser();
		productPage=PageFactory.initElements(BasePage.driver,ProductPage.class);
		productPage.nevigateToURL(BasePage.getTestURL("productPageAssociateWithReferrerUrlOnlyAndRefUrlPlusLibCardURL")+BasePage.siteID);   
	}

	@When("^I Click on Go To Referrer URL And Library Card Only Link$")
	public void i_Click_on_Go_To_Referrer_URL_And_Library_Card_Only_Link() throws Throwable 
	{
		libraryPage=PageFactory.initElements(BasePage.driver,LibraryCardPage.class);
		libraryPage.goToLibraryPage();
	}

	@Then("^I Should Have The Library Card Entry Form$")
	public void i_Should_Have_The_Library_Card_Entry_Form() throws Throwable 
	{
		libraryPage.validateLibraryCardPage();
	}

	@Then("^I Insert Library Card Name In the Library card name Field$")
	public void i_Insert_Library_Card_Name_In_the_Library_card_name_Field() 
	{
		int x=BasePage.readProperty("prefix").length();
		int cardNumber=x+Integer.parseInt(BasePage.readProperty("length"));
		libraryPage.enterLibraryCard(BasePage.readProperty("prefix")+cardNumber);
	}

	@Then("^I Click lOG in button$")
	public void i_Click_lOG_in_button()
	{
		libraryPage.clickLogInButton();
	}

	@Then("^I Should Have the Scholastic Digital Manager Page$")
	public void i_Should_Have_the_Scholastic_Digital_Manager_Page() 
	{
	    
	}
}
