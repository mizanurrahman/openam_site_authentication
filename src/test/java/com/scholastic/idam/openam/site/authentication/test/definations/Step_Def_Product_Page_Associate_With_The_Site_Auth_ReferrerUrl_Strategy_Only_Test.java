package com.scholastic.idam.openam.site.authentication.test.definations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.idam.openam.site.main.BasePage;
import com.scholastic.idam.openam.site.main.pages.ProductPage;
import com.scholastic.idam.openam.site.main.support.HelperClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class Step_Def_Product_Page_Associate_With_The_Site_Auth_ReferrerUrl_Strategy_Only_Test 
{
	public WebDriver driver=null;
	public HelperClass helperClass=new HelperClass();
	public static ProductPage productPage;
	public static final String STRATREGY="referrerUrlOnlyStrategy";

	@Given("^I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API for the Product Page of ReferrerUrl Strategy Only$")
	public void i_Am_Creating_Open_Am_Site_Authentication_ID_using_IAM_Site_Authentication_API_for_the_Product_Page_of_ReferrerUrl_Stratregy_Only() 
	{
		helperClass=new HelperClass();
		BasePage.siteID=helperClass.createSiteAuthenticationOnlyWithReferrerUrlStrategyProductPageTest(STRATREGY); 
	}

	@When("^I Invoke Open-Am Authentication ReferrerUrl and Library Card Strategy Only URL With Site Authentication ID$")
	public void i_Invoke_Open_Am_Authentication_Product_Page_ReferrerUrl_Strategy_Only_APT_With_Site_Authentication_ID_Using_Browser()
	{
		System.out.println("Product Page - ReferrerUrlOnly Strategy");
		BasePage.openBrowser();
		productPage=PageFactory.initElements(BasePage.driver,ProductPage.class);
		productPage.nevigateToURL(BasePage.getTestURL("productPageAssociateWithReferrerUrlOnlyAndRefUrlPlusLibCardURL")+BasePage.siteID); 
	}

	@When("^I Click on Go To The Link Of Library Card$")
	public void i_Click_on_Go_To_Referrer_URL_Only_Link() 
	{
		productPage.goToProductPage();
	}
	
	@Then("^I Should Get the Scholastic Digital Manager Page$")
	public void i_Should_Get_the_Scholastic_Digital_Manager_Page() 
	{
	   
	}
}
