package com.scholastic.idam.openam.site.authentication.test.definations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.idam.openam.site.main.BasePage;
import com.scholastic.idam.openam.site.main.pages.LibraryCardPage;
import com.scholastic.idam.openam.site.main.pages.LoginPage;
import com.scholastic.idam.openam.site.main.support.HelperClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class Step_Def_Site_Auth_ReferrerURL_And_Library_Card_Stratregy_LogIn_Test 
{
	public WebDriver driver=null;
	public HelperClass helperClass=new HelperClass();
	public String cookie;
	public static LoginPage loginPage;
	public LibraryCardPage libraryPage=null;
	public static final String STRATREGY="referrerUrlAndLibraryCardStrategy";
	
	@Given("^I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API for referrer URL and Library Card$")
	public void i_Am_Creating_Open_Am_Site_Authentication_ID_using_IAM_Site_Authentication_API_for_referrer_URL_and_Library_Card() throws Throwable 
	{
		helperClass=new HelperClass();
		BasePage.siteID=helperClass.createSiteAuthenticationOnlyWithReferrerUrlAndLibraryCardStrategyTest(STRATREGY);
	}
	
	@When("^I Invoke Open-Am Authentication Referrer Url and Library Card Only Strategy URL With Site ID$")
	public void i_Invoke_Open_Am_Authentication_Referrer_Url_and_Library_Card_Only_Strategy_URL_With_Site_ID_Using_Firefox_Browser() throws Throwable
	{
		System.out.println("ReferrerUrlAndLibraryCard Only Strategy Login");
		BasePage.openBrowser();
		libraryPage=PageFactory.initElements(BasePage.driver,LibraryCardPage.class);
		libraryPage.nevigateToURL(BasePage.getTestURL("openAMReferrerURLAndLibraryCard")+BasePage.siteID);
	}
	

	@When("^I Click on Go To Library Card Link$")
	public void i_Click_on_Go_To_Library_Card_Link() throws Throwable
	{
		libraryPage.goToLibraryPage();
	}

	@Then("^I Should See The Library Card Entry Form$")
	public void i_Should_See_The_Library_Card_Entry_Form() throws Throwable 
	{
		libraryPage=PageFactory.initElements(BasePage.driver,LibraryCardPage.class);
		libraryPage.validateLibraryCardPage();
	}
	

	@Then("^I Enter Library Card Number In the Library card number Field$")
	public void after_I_Enter_Library_Card_Number_In_the_Library_card_number_Field() throws Throwable 
	{
		int x=BasePage.readProperty("prefix").length();
		int cardNumber=x+Integer.parseInt(BasePage.readProperty("length"));
		libraryPage.enterLibraryCard(BasePage.readProperty("prefix")+cardNumber);
	}
	

	@Then("^Click lOG in button$")
	public void click_lOG_in_button() throws Throwable 
	{
		libraryPage.clickLogInButton();
	}
	
	@Then("^I Should See Log out and Save and Reset button and Site ID after Successfully log in$")
	public void i_should_redirect_to_the_logged_in_page() 
	{
		loginPage=PageFactory.initElements(BasePage.driver,LoginPage.class);
		loginPage.validateLogoutButton();
		loginPage.validateSaveButton();
		loginPage.validateResetButton();
		loginPage.validateSiteID(BasePage.siteID);
	}
	
	@Then("^I should Get Cookie From the Browser$")
	public void i_should_Get_Cookie_From_the_Browser()
	{	
		cookie=loginPage.getCookie();
	}

	@When("^I Will Call Open AM Attribute Api along with the Browser Cookies$")
	public void i_Will_Call_Open_AM_Attribute_Api_with_the_Browser_Cookies()
	{
		helperClass.setCookieValue(cookie);
	}
	
	@Then("^I should get Those referrer Url and Library Card strategy attributes name and value in response$")
	public void i_should_get_Thoese_attributes_name_and_Value_in_Respons() throws InterruptedException 
	{
		helperClass.validateCookiesAttributes(BasePage.siteID,STRATREGY);
	}
}
