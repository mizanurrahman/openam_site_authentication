package com.scholastic.idam.openam.site.authentication.test.definations;


import com.scholastic.idam.openam.site.main.BasePage;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class BaseTest
{
 		
 	@Before
 	 public void beforeTest()
 	 {
 		System.out.println("Starting the Scenarion ..................");
 		BasePage.init();
 	 }
 	
 	@After
 	public void afetrTest() throws InterruptedException
 	{
 		BasePage.closeBrowser();
 		System.out.println("Ending the Scenario .......................");
 	}
}

