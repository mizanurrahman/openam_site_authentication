package com.scholastic.idam.openam.site.authentication.test.definations;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

	@RunWith(Cucumber.class)
	@CucumberOptions
	(		
		features ={"src/test/resources/FeatureFiles"}
	)

	public class IntegrationTestRunnerClass_Test
	{

	}


