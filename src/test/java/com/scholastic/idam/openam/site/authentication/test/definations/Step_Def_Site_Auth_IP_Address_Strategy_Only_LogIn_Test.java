package com.scholastic.idam.openam.site.authentication.test.definations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.idam.openam.site.main.BasePage;
import com.scholastic.idam.openam.site.main.pages.LoginPage;
import com.scholastic.idam.openam.site.main.support.HelperClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Step_Def_Site_Auth_IP_Address_Strategy_Only_LogIn_Test 
{
	public WebDriver driver=null;
	public HelperClass helperClass=new HelperClass();
	public static LoginPage loginPage;
	public String cookie;
	public static final String STRATREGY="ipAddressOnlyStrategy";
		
	@Given("^I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API$")
	public void i_have_given_opem_am_url_with_site_id()
	{
		helperClass=new HelperClass();
		BasePage.siteID=helperClass.createSiteAuthenticationOnlyWithIpAddressStrategyTest(STRATREGY);
	}
	
	@When("^I Invoke Open-Am Authentication IP Address Only URL with Site Authentication ID$")
	public void i_Invoke_Open_Am_Authentication_IP_Address_Only_URL_with_Site_Authentication_ID_Using_Browser() 
	{
		System.out.println("ipAddressOnly Strategy");
		BasePage.openBrowser();
		loginPage=PageFactory.initElements(BasePage.driver,LoginPage.class);
		loginPage.nevigateToURL(BasePage.getTestURL("openAMSiteAuthURL")+BasePage.siteID);
	}

	
	@Then("^I Should See Log out and Save and Reset button and Site ID after Successfully Log in$")
	public void i_should_redirect_to_the_logged_in_page() 
	{
		loginPage.validateLogoutButton();
		loginPage.validateSaveButton();
		loginPage.validateResetButton();
		loginPage.validateSiteID(BasePage.siteID);
	}
	
	@Then("^I should Get Cookie From Browser$")
	public void i_sould_Get_Cookie_From_Browser()
	{
		cookie=loginPage.getCookie();
		
	}

	@When("^I Will Call Open AM Attribute Api with the Browser Cookies$")
	public void i_Will_Call_Open_AM_Attribute_Api_with_the_Browser_Cookies()
	{
		helperClass.setCookieValue(cookie);
	}

	@Then("^I should get Those ipAddress Only Strategy attributes name and value in response$")
	public void i_should_get_Thoese_attributes_name_and_Value_in_Respons() throws InterruptedException 
	{
		helperClass.validateCookiesAttributes(BasePage.siteID,STRATREGY);
}
}
