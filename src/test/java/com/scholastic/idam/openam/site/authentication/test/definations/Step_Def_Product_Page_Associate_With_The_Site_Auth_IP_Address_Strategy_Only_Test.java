package com.scholastic.idam.openam.site.authentication.test.definations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.idam.openam.site.main.BasePage;
import com.scholastic.idam.openam.site.main.pages.ProductPage;
import com.scholastic.idam.openam.site.main.support.HelperClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Step_Def_Product_Page_Associate_With_The_Site_Auth_IP_Address_Strategy_Only_Test 
{
	public WebDriver driver=null;
	public HelperClass helperClass=new HelperClass();
	public static ProductPage productPage;
	public static final String STRATREGY="ipAddressOnlyStrategy";
	
	@Given("^I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API for the Product Page of IP Address Strategy Only$")
	public void i_Am_Creating_Open_Am_Site_Authentication_ID_using_IAM_Site_Authentication_API_for_the_Product_Page_of_IP_Address_Stratregy_Only()
	{
		helperClass=new HelperClass();
		BasePage.siteID=helperClass.createSiteAuthenticationOnlyWithIpAddressStrategyTest(STRATREGY);  
	}

	@When("^I Invoke Open-Am Authentication IP Address Only Product Page URL with Site Authentication ID$")
	public void i_Invoke_Open_Am_Authentication_IP_Address_Only_Product_Page_URL_with_Site_Authentication_ID_Using_Browser()
	{
		System.out.println("Product Page - iOnly Strategy");
		BasePage.openBrowser();
		productPage=PageFactory.initElements(BasePage.driver,ProductPage.class);
		productPage.nevigateToURL(BasePage.getTestURL("productPageAssociateWithIPAddressOnlyURL")+BasePage.siteID);
	}
	
	@Then("^I Should See the Scholastic Digital Manager Page$")
	public void i_Should_See_the_Scholastic_Digital_Manager_Page() 
	{
	   
	}
}
