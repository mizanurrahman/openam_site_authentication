package com.scholastic.idam.openam.site.authentication.test.definations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.idam.openam.site.main.BasePage;
import com.scholastic.idam.openam.site.main.pages.LibraryCardPage;
import com.scholastic.idam.openam.site.main.pages.LoginPage;
import com.scholastic.idam.openam.site.main.support.HelperClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Step_Def_Site_Auth_ReferrerURL_Strategy_Only_LogIn_Test 
{
	public WebDriver driver=null;
	public HelperClass helperClass=new HelperClass();
	public String cookie;
	public static LoginPage loginPage;
	public LibraryCardPage libraryPage=null;
	public static final String STRATREGY="referrerUrlOnlyStrategy";
	
	@Given("^I Am Creating Open-Am Site Authentication ID using IAM Site Authentication API for referrer URL$")
	public void i_Am_Creating_Open_Am_Site_Authentication_ID_using_IAM_Site_Authentication_API_for_referrer_URL() throws Throwable 
	{
		helperClass=new HelperClass();
		BasePage.siteID=helperClass.createSiteAuthenticationOnlyWithReferrerUrlStrategyTest(STRATREGY);
	}
	
	@When("^I Invoke Open-Am Authentication ReferrerUrl Strategy Only With Site Authentication ID$")
	public void i_Invoke_Open_Am_Authentication_ReferrerUrl_Stratregy_Only_With_Site_Authentication_ID_Using_Firefox_Browser( ) throws Throwable
	{	
		System.out.println("ReferrerUrlOnly Strategy Login");
		BasePage.openBrowser();
		loginPage=PageFactory.initElements(BasePage.driver,LoginPage.class);
		loginPage.nevigateToURL(BasePage.getTestURL("openAMReferrerURLAndLibraryCard")+BasePage.siteID);
	}
	
	@When("^I Click on Go To Referrer URL Link$")
	public void i_Click_on_Go_To_Referrer_URL_Link() throws Throwable
	{
		loginPage.goToLoginPage();
	}
	
	@Then("^I will See Log out and Save and Reset button and Site ID after Successfully log in$")
	public void i_will_redirect_to_the_logged_in_page() 
	{
		loginPage=PageFactory.initElements(BasePage.driver,LoginPage.class);
		loginPage.validateLogoutButton();
		loginPage.validateSaveButton();
		loginPage.validateResetButton();
		loginPage.validateSiteID(BasePage.siteID);
	}
	
	@Then("^I should Have Cookies From the Browser$")
	public void i_sould_Have_Cookies_From_the_Browser()
	{	
		cookie=loginPage.getCookie();
	}

	@When("^I Am Going to Call Open AM Attribute Api along with the Browser Cookies$")
	public void i_Am_Going_to_Call_Open_AM_Attribute_Api_with_the_Browser_Cookies()
	{
		helperClass.setCookieValue(cookie);
	}
	
	@Then("^I should get Those referrer Url strategy attributes name and value in response$")
	public void i_should_get_Thoese_attributes_name_and_Value_in_Respons() throws InterruptedException 
	{
		helperClass.validateCookiesAttributes(BasePage.siteID,STRATREGY);
	}

}
